package com.kiwicampus.ep.java.test;

import com.kiwicampus.ep.java.util.ModelUtils;
import org.junit.Test;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ModelUtilsTests {

    @Test
    public void testCopy() {
        A a = new A();
        a.a = 1;

        Aa copy = ModelUtils.copy(a).into(Aa.class);
        assert copy.getA() == a.getA();
        assert copy.getAa() == 0;
        copy.aa = 2;

        ABa copy2 = new ABa();
        ModelUtils.copy(copy).into(copy2);
        // Since in copy2 this is of different type, should not have copied
        // Hence it should remain 0
        assert copy2.getA() == 0.0f;
        System.out.println("copy2.getA() = " + copy2.getA());
        assert copy2.getB() == 0;
        assert copy2.getAa() == 2;
    }

    @Test
    public void testWhere() {
        Aa a = new Aa();
        a.a = 1;
        a.aa = 2;

        Aa copy1 = ModelUtils.copy(a).where(val -> ((Integer) val) > 1).into(Aa.class);
        assert copy1.a != 1;
        assert copy1.aa == 2;

        Nullable o1 = new Nullable();
        Nullable o2 = new Nullable();
        o2.setObj(new Object());

        ModelUtils.copy(o1).excludeNulls().into(o2);
        assert o2.getObj() != null;
    }

    @Test
    public void testIncludeExclude() {
        Aa a = new Aa();
        a.a = 1;
        a.aa = 2;

        Aa a2 = ModelUtils.copy(a).fields("a").into(Aa.class);

        assert a2.a == 1;
        assert a2.aa != 2;


        Aa a3 = ModelUtils.copy(a).excludeFields("a").into(Aa.class);
        assert a3.a != 1;
        assert a3.aa == 2;
    }

    public static class Nullable {
        private Object obj;

        public Object getObj() {
            return obj;
        }

        public void setObj(Object obj) {
            this.obj = obj;
        }
    }

    public static class A {
        protected int a;

        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }
    }

    public static class Aa extends A {
        private int aa;

        public int getAa() {
            return aa;
        }

        public void setAa(int aa) {
            this.aa = aa;
        }
    }

    public static class ABa {
        private float a;
        private int b;
        private int aa;

        public float getA() {
            return a;
        }

        public void setA(float a) {
            this.a = a;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }

        public int getAa() {
            return aa;
        }

        public void setAa(int aa) {
            this.aa = aa;
        }
    }


}
