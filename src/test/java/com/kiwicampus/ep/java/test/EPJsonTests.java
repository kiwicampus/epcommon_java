package com.kiwicampus.ep.java.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kiwicampus.ep.java.json.EPJson;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EPJsonTests {

    @Test
    public void testMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("string", "some string");
        map.put("number", 1);
        map.put("char", 'a');
        Map<String, Object> epMap = EPJson.map("string", "some string", "number", 1, "char", 'a');
        assert map.equals(epMap);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("Completed orders", Arrays.asList("$sum", 1));

        Map<String, Object> epMap2 = EPJson.map(
                "Completed orders", EPJson.array("$sum", 1));
        assert map2.equals(epMap2);
    }

    @Test
    public void testObject() {
        List<String> tokens = Arrays.asList("t1", "t2", "t3");
        String msg = "Some message";
        ObjectNode epObject =
                EPJson.object("registration_ids", tokens,
                        "data", EPJson.object("message", msg, "title", "Kiwi"));

        final ObjectMapper mapper = new ObjectMapper();
        final ObjectNode object = mapper.createObjectNode();
        final ObjectNode inner = mapper.createObjectNode();
        inner.set("message", mapper.convertValue(msg, JsonNode.class));
        inner.set("title", mapper.convertValue("Kiwi", JsonNode.class));
        object.set("registration_ids", mapper.convertValue(tokens, JsonNode.class));
        object.set("data", inner);
        assert object.equals(epObject);
    }
}