package com.kiwicampus.ep.java.test;

import com.kiwicampus.ep.java.util.Util;
import org.junit.Test;

public class UtilTests {
    @Test
    public void testAsync() {
        String a = null;
        Runnable runnable = () -> {
            //noinspection ResultOfMethodCallIgnored,ConstantConditions
            a.length();
        };

        int[] flag = new int[1];
        Util.setAsyncErrorHandler(t -> {
            t.printStackTrace();
            System.out.println("Hellow!!!!!!");
            flag[0] = 1;
        });

        Util.runAsync(runnable);

        do {
            Util.waitThread(1000);
        } while (flag[0] == 0);
    }
}