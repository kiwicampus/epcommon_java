package com.kiwicampus.ep.java;

/**
 * Created by jasonoviedo on 12/29/16.
 */
public interface EPIdObject {
    String getId();

    void setId(String id);
}
