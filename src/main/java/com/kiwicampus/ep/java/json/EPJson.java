package com.kiwicampus.ep.java.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * This class eases working with Json by with a simple convention for key, value pairs.
 * Key, values are expected as an object array, in this way:
 * <p>k1, v1, ...., kn, vn
 * <p>
 * Created by Jason Oviedo on 18/01/2016.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class EPJson {
    private static Logger logger = LoggerFactory.getLogger(EPJson.class);

    private static EPJsonMapper epMapper = new EPJsonMapper();

    /**
     * Get the ObjectMapper used to serialize and deserialize objects to and from JSON values.
     * <p>
     * This can be set to a custom implementation using EPJson.setObjectMapper.
     *
     * @return the ObjectMapper currently being used
     */
    private static ObjectMapper jacksonMapper() {
        return epMapper.mapper();
    }

    /**
     * Inject the object mapper to use.
     * <p>
     * This is intended to be used when the library starts up.  By default, the library will inject its own object mapper here,
     * but this mapper can be overridden either by a custom module.
     */
    public static void setMapper(EPJsonMapper mapper) {
        logger.warn("EPObjectMapper changed for EPJson");
        epMapper = mapper;
    }
    //************************************************************************
    // KEY, VALUE ARRAY TO JSON
    //************************************************************************

    /**
     * Equivalent to {@link #object(Object...) object(...).toString()}
     *
     * @param params Key, values in k1, v1, ...., kn, vn form
     * @return Json as String
     */
    @Nonnull
    public static String string(Object... params) {
        return epMapper.string(params);
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static Map<String, Object> map(Object... params) {
        return epMapper.map(params);
    }

    /**
     * Transforms the list of key,value pairs into a map of type Map&lt;String, T&gt;
     * <p>
     * WARNING: This is an unchecked cast. Caller has responsibility to ensure
     * casted type is compatible.
     *
     * @param params List of key, value pairs
     * @param <T>    Type of data being contained
     * @return Casted map
     */
    @SuppressWarnings("unchecked")
    public static <T> Map<String, T> castedMap(Object... params) {
        return epMapper.castedMap(params);
    }

    /**
     * Creates a Json object with the parameters. Id keys are transformed to mongo format (_id)
     *
     * @param params Key, values in k1, v1, ...., kn, vn form
     * @return Json object
     */
    @Nonnull
    public static ObjectNode object(Object... params) {
        return epMapper.object(params);
    }

    /**
     * Transforms the array of elements into a json array. Only basic types are supported
     *
     * @param params Array of elements to transform. Only basic types are supported
     * @return ArrayNode with the transformed elements
     */
    public static ArrayNode array(Object... params) {
        return epMapper.array(params);
    }

    //************************************************************************
    // TO/FROM JSON API
    //************************************************************************

    /**
     * Transforms iterable to list. Useful for processing ArrayNodes.
     * Equivalent to {@link #toPojo(Object, Class) toPojo(object, List.class)}
     *
     * @param data Iterable to transform. Specially useful for transforming ArrayNodes
     * @return List of objects represented as maps
     */
    public static List<Map<String, Object>> toList(Iterable<?> data) {
        return epMapper.toList(data);
    }

    /**
     * Transforms iterable to list. Each element is mapped by applying
     * {@link #toPojo(Object, Class) toPojo(object, clazz)} on it.
     * Useful for processing ArrayNodes.
     * Equivalent to {@link #toPojo(Object, Class) toPojo(object, List.class)} and then
     * applying {@link #toPojo(Object, Class) toPojo(element, clazz)} on each element.
     *
     * @param data Iterable to transform. Specially useful for transforming ArrayNodes
     * @return List of objects represented as maps
     */
    public static <T> List<T> toList(Iterable<?> data, Class<T> clazz) {
        return epMapper.toList(data, clazz);
    }


    /**
     * Transforms object to map. Equivalent to {@link #toPojo(Object, Class) toPojo(object, Map.class)}
     *
     * @param data Object
     * @return Map with data
     */
    public static Map<String, Object> toMap(Object data) {
        return epMapper.toMap(data);
    }

    /**
     * Convert an object to JsonNode.
     *
     * @param data Value to convert in Json.
     */
    public static JsonNode toJson(Object data) {
        return epMapper.toJson(data);
    }

    /**
     * Converts map or json to Pojo
     *
     * @param object Value to be transformed
     * @param clazz  Class of target POJO
     * @param <T>    Type of target Pojo
     * @return Converted value
     */
    public static <T> T toPojo(Object object, Class<T> clazz) {
        return epMapper.toPojo(object, clazz);
    }


    /**
     * Convert a JsonNode to a Java value
     *
     * @param json  Json value to convert.
     * @param clazz Expected Java value type.
     */
    public static <A> A fromJson(JsonNode json, Class<A> clazz) {
        try {
            return jacksonMapper().treeToValue(json, clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Parses and transforms a stream containing json into a bean
     *
     * @param jsonStream Json stream
     * @param clazz      Class of response
     * @param <A>        Type of response
     * @return Bean
     */
    public static <A> A fromJson(InputStream jsonStream, Class<A> clazz) {
        return fromJson(parse(jsonStream), clazz);
    }

    /**
     * Parses and transforms a byte array containing json into a bean
     *
     * @param jsonBytes Byte array
     * @param clazz     Class of response
     * @param <A>       Type of response
     * @return Bean
     */
    public static <A> A fromJson(byte[] jsonBytes, Class<A> clazz) {
        return fromJson(parse(jsonBytes), clazz);
    }

    //************************************************************************
    // TO STRING
    //************************************************************************

    /**
     * Serializes object to json string
     *
     * @param o              Object (can be json already)
     * @param prettyPrint    True for pretty print
     * @param escapeNonASCII True to scape non ascii chars
     * @return Serialized json
     */
    public static String toString(Object o, boolean prettyPrint, boolean escapeNonASCII) {
        try {
            ObjectWriter writer = jacksonMapper().writer();
            if (prettyPrint) {
                writer = writer.with(SerializationFeature.INDENT_OUTPUT);
            }
            if (escapeNonASCII) {
                writer = writer.with(JsonGenerator.Feature.ESCAPE_NON_ASCII);
            }
            return writer.writeValueAsString(o);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Convert any Object to its json string representation.
     */
    public static String toString(Object data) {
        return toString(data, false, false);
    }

    /**
     * Convert any Object to its json  to its string representation, escaping non-ascii characters.
     */
    public static String toAsciiString(Object data) {
        return toString(data, false, true);
    }

    /**
     * Convert any Object to its json string representation.
     */
    public static String toPrettyPrint(Object data) {
        return toString(data, true, false);
    }

    //************************************************************************
    // PARSING
    //************************************************************************

    /**
     * Parse a String representing a json, and return it as a JsonNode.
     */
    public static JsonNode parse(String src) {
        try {
            return jacksonMapper().readTree(src);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    /**
     * Parse a InputStream representing a json, and return it as a JsonNode.
     */
    public static JsonNode parse(InputStream src) {
        try {
            return jacksonMapper().readTree(src);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    /**
     * Parse a byte array representing a json, and return it as a JsonNode.
     */
    public static JsonNode parse(byte[] src) {
        try {
            return jacksonMapper().readTree(src);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
