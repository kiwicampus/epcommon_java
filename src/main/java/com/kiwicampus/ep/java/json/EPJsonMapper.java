package com.kiwicampus.ep.java.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Iterables;
import com.kiwicampus.ep.java.util.Pair;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Same functionality offered before by EPJson, only now it can be configured depending if
 * id fields should be transformed or not to _id
 * <p>
 * This is particularly useful for Mongo conversition
 * <p>
 * Created by jasonoviedo on 3/22/17.
 */
@SuppressWarnings("WeakerAccess")
public class EPJsonMapper {

    private static final Set<Class<?>> simpleTypes = new HashSet<>();
    private static final ObjectMapper defaultObjectMapper = newDefaultMapper();
    private volatile ObjectMapper objectMapper = null;

    static {
        simpleTypes.add(Boolean.class);
        simpleTypes.add(Character.class);
        simpleTypes.add(Byte.class);
        simpleTypes.add(Short.class);
        simpleTypes.add(Integer.class);
        simpleTypes.add(Long.class);
        simpleTypes.add(Float.class);
        simpleTypes.add(Double.class);
        simpleTypes.add(Void.class);

        //We consider String as non transformable
        simpleTypes.add(String.class);
    }


    private static ObjectMapper newDefaultMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    public ObjectMapper mapper() {
        if (objectMapper == null) {
            return defaultObjectMapper;
        } else {
            return objectMapper;
        }
    }

    protected void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    /**
     * Intended to be overridden
     *
     * @param keyValuePair The field name and value as received
     * @return Transformed field name and value
     */
    protected Pair<String, Object> applyCustomMapping(Pair<String, Object> keyValuePair) {
        return keyValuePair;
    }

    public EPJsonMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public EPJsonMapper() {
    }

    /**
     * Equivalent to {@link #object(Object...) object(...).toString()}
     *
     * @param params Key, values in k1, v1, ...., kn, vn form
     * @return Json as String
     */
    @Nonnull
    public String string(Object... params) {
        return object(params).toString();
    }

    /**
     * Transforms to map, whatever it is. It can be an object or a {@link ObjectNode}
     *
     * @param data Data to transform
     * @return Map
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> toMap(Object data) {
        Map map = mapper().convertValue(data, Map.class);
        return new HashMap<>(map);
    }

    /**
     * Transforms to json whatever it receives.
     *
     * @param data Object, map, collection or array
     * @return Json representation
     */
    public JsonNode toJson(Object data) {
        try {
            return mapper().valueToTree(data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Converts map or json to Pojo
     *
     * @param object Valuee to be transformed
     * @param clazz  Class of target POJO
     * @param <T>    Type of target Pojo
     * @return Converted value
     */
    public <T> T toPojo(Object object, Class<T> clazz) {
        return mapper().convertValue(object, clazz);
    }


    /**
     * Creates a Json object with the parameters. Id keys are transformed to mongo format (_id)
     *
     * @param params Key, values in k1, v1, ...., kn, vn form
     * @return Json object
     */
    @Nonnull
    public ObjectNode object(Object... params) {
        //We are confident in the cast because a map
        //produces an object for sure
        return (ObjectNode) toJson(map(params));
    }

    /**
     * Create an ArrayNode out of the provided params
     *
     * @param params Var args
     * @return ArrayNode
     */
    @Nonnull
    public ArrayNode array(Object... params) {
        //Cast should be safe, array should produce json array
        return (ArrayNode) toJson(params);
    }


    /**
     * Creates a map out of the parameters
     *
     * @param params Var args
     * @return Map
     */
    @Nonnull
    public Map<String, Object> map(Object... params) {
//        return toMap(object(params));
        return transformToMap(true, params);
    }

    /**
     * Creates a map out of parameters. Result is automatically casted to the assigned variable
     * This method is just syntactic sugar and overrides default compiler warnings,
     * which can result in runtime exceptions. Use with care
     *
     * @param params var args
     * @param <T>    Type of Map
     * @return Casted map
     */
    @SuppressWarnings("unchecked")
    public <T> Map<String, T> castedMap(Object... params) {
        return (Map<String, T>) map((Object[]) params);
    }

    @NotNull
    private Map<String, Object> transformToMap(boolean deep, Object[] params) {
        Map<String, Object> transformedMap = new HashMap<>();
        if (params.length % 2 != 0)
            throw new IllegalArgumentException("Params provided are not correct. Keys and values do not match: " + Arrays.asList(params));
        for (int i = 0; i < params.length - 1; i += 2) {

            String fieldName = params[i].toString();
            if (fieldName == null)
                throw new IllegalArgumentException("Object/map keys cannot be null");

            Object p = params[i + 1];
            Pair<String, Object> pair = applyCustomMapping(new Pair<>(fieldName, p));

            fieldName = pair.getKey();
            p = pair.getValue();

            //Transform
            Object transformed;

            if (p == null)
                transformed = null;
            else if (p.getClass().isEnum())
                transformed = p.toString();
            else if (ObjectNode.class.isInstance(p))
                transformed = toMap(p);
            else if (Iterable.class.isInstance(p))
                transformed = deep ? mapper().convertValue(p, ArrayList.class)
                        : Arrays.asList(Iterables.toArray((Iterable) p, Object.class));
            else if (p.getClass().isArray())//To array
                transformed = deep ? mapper().convertValue(p, ArrayList.class)
                        : Arrays.asList((Object[]) p);
                //If deep map and not simple type
            else if (deep && !simpleTypes.contains(p.getClass()))
                transformed = toMap(p);
            else
                transformed = p;

            transformedMap.put(fieldName, transformed);
        }

        return transformedMap;
    }

    private Map<String, Object> shallowMap(Object... params) {
        return transformToMap(false, params);
    }

    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> toList(Iterable<?> data) {
        List<Map> res = toList(data, Map.class);
        //noinspection Convert2MethodRef
        Stream<HashMap<String, Object>> hashMapStream = res
                .stream()
                .map(elem -> new HashMap<String, Object>(elem));
        return hashMapStream
                .collect(Collectors.toList());
    }

    public <T> List<T> toList(Iterable<?> data, Class<T> clazz) {
        List<?> list = mapper().convertValue(data, List.class);
        return list
                .stream()
                .map(elem -> toPojo(elem, clazz))
                .collect(Collectors.toList());
    }
}
