package com.kiwicampus.ep.java.util;

/**
 * Utility, some libraries implement this functionality, but I've found
 * not all of them are always 100% what you need
 * @param <K> Key Type
 * @param <V> Value type
 */
public class Pair<K, V> {
    private K key;
    private V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public static <K, V> Pair<K, V> of(K key, V value) {
        return new Pair<>(key, value);
    }
}
