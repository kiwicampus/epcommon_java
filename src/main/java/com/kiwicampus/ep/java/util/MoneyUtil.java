package com.kiwicampus.ep.java.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jasonoviedo on 10/10/16.
 */

public class MoneyUtil {
    public static String formatMoney(double money) {
        DecimalFormat df = new DecimalFormat("0.##");
        return (money < 0 ? "-$" : "$") + df.format(Math.abs(money));
    }


    public static final List<String> zeroDecimalCurrencies = Arrays.asList("BIF","CLP","DJF","GNF","JPY","KMF","KRW","MGA","PYG","RWF","VND","VUV","XAF","XOF","XPF");

    public static int toMoneyInt(double price, String currency) {
        int multiplier = zeroDecimalCurrencies.contains(currency) ? 1 : 100;
        BigDecimal result = new BigDecimal(price).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(multiplier));
        return result.intValue();
    }

    public static double fromMoneyInt(int remainderAmount, String currency) {
        double multiplier = zeroDecimalCurrencies.contains(currency) ? 1.00 : 100.00;
        return remainderAmount/ multiplier;
    }
}
