package com.kiwicampus.ep.java.util;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Small container to represent a time span. A similar class must exist somewhere, if at
 * any time it seems to be a good idea to remove this one, just go ahead ;)
 *
 * Created by jasonoviedo on 2/23/17.
 */
public class TimeSpan {
    public long start;
    public long end;

    public static TimeSpan of(ZonedDateTime start, int length, ChronoUnit units) {
        TimeSpan res = new TimeSpan();
        res.start = start.toInstant().toEpochMilli();
        res.end = start.plus(length, units).toInstant().toEpochMilli();
        return res;
    }
}
