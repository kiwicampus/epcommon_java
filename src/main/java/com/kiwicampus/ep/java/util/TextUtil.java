package com.kiwicampus.ep.java.util;

/**
 * Created by jasonoviedo on 9/7/16.
 */
public class TextUtil {

    public static boolean isEmpty(String text) {
        return text == null || text.equals("");
    }

    public static boolean isNotEmpty(String text) {
        return !isEmpty(text);
    }

    public static String firstToLower(String s) {
        return Character.toLowerCase(s.charAt(0)) + s.substring(1);
    }

    public static String firstToUpper(String s) {
        return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }
}
