package com.kiwicampus.ep.java.util;

import com.kiwicampus.ep.java.exceptions.KiwiException;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Utility class to easily copy an object into a another, either a new or an existing instance.
 * Individual fields can be specified to be included or excluded.
 *
 * @param <T>
 */
@SuppressWarnings("WeakerAccess")
public class ModelUtils<T> {
    private static final List<String> EXCLUDE_METHODS = Arrays.asList("class", "password");

    /**
     * Main access point to this utility class. The way it is intended to work is usually in the form:
     * <code>ModelUtils.copy(obj).into(OtherClass.class)</code>
     * This base use case will just create a new instance of the OtherClass class and copy all matching
     * fields. Matching is performed by getter/setter correspondence. Of course, an isField() boolean
     * will get matched to a setField(val) on the target.
     * <p>
     * The copy can be further customized by the other available methods in ModelCopy (the intermediate)
     * class used for the process. You can select exactly what fields to use using fields(... fieldNames),
     * or exclude some using excludeFields(... fieldNames). There are some other more sophisticated methods too.
     *
     * <b>Important:</b> By default, a field 'password' will not be included
     *
     * @param source Instance whose values will be copied from
     * @param <T>    Class of source instance
     * @return Intermediate obj used to customize the copy
     */
    public static <T> ModelCopy<T> copy(T source) {
        return new ModelCopy<>(source);
    }

//    @SuppressWarnings({"NestedTryStatement", "ThrowCaughtLocally"})
//    @Nonnull
//    public static <SUPER, T> T copy(Class<T> clazz, T instance, SUPER from) {
//        return copy(clazz, instance, from, null, null);
//    }

    /**
     * Intermediate class for model copies. for details
     *
     * @param <FROM> Type of object being copied
     * @see ModelUtils#copy(Object)
     */
    @SuppressWarnings("unused")
    public static class ModelCopy<FROM> {
        private final FROM from;
        private Set<String> includeFields = new HashSet<>();
        private Set<String> excludeFields = new HashSet<>();
        private Predicate<Object> where = obj -> true;


        private ModelCopy(FROM from) {
            this.from = from;
        }


        //*********************************************************************
        // PUBLIC API
        //*********************************************************************

        /**
         * Include <b>ONLY</b> the fields in this list
         *
         * @param fields List of field name
         * @return Itself for further customization
         */
        public ModelCopy<FROM> fields(String... fields) {
            for (String field : fields)
                if (excludeFields.contains(field))
                    throw new KiwiException("Include field is already marked as excluded: " + field);
            includeFields.addAll(Arrays.asList(fields));
            return this;
        }

        /**
         * Exclude all of the fields in the list
         *
         * @param fields List of field name
         * @return Itself for further customization
         */
        public ModelCopy<FROM> excludeFields(String... fields) {
            for (String field : fields)
                if (includeFields.contains(field))
                    throw new KiwiException("Exclude field is already marked as included: " + field);
            excludeFields.addAll(Arrays.asList(fields));
            return this;
        }

        /**
         * Sets a predicate to be evaluted on a per value basis. If invoked several times,
         * the resulting predicate will be an {@link Predicate#and(Predicate)} of each passed
         * predicate.
         *
         * @param predicate Predicate to evaluate
         * @return Itself for further customization
         */
        public ModelCopy<FROM> where(Predicate<Object> predicate) {
            where = where.and(predicate);
            return this;
        }

        /**
         * Exclude null values. This method is exactly the same as <code>where(Objects::nonNull)</code>
         *
         * @return Itself for further customization
         */
        public ModelCopy<FROM> excludeNulls() {
            return where(Objects::nonNull);
        }


        /**
         * Do perform the copy into a new instance of the provided class
         *
         * @param clazz A new instance of this class will be created by clazz.newInstance()
         * @param <RES> Type of the new instance
         * @return The newly created instance with the copied values
         * @throws KiwiException    if the provided class instance cannot be created
         * @throws RuntimeException if for any reason the copy fails (rare).
         */
        public <RES> RES into(Class<RES> clazz) {
            excludeFields.addAll(EXCLUDE_METHODS);
            return create(clazz, from);
        }

        /**
         * Do perform the copy into the provided instance
         *
         * @param instance Instance to copy the values into
         * @param <RES>    Type of the instance
         * @return The provided instance after copying the values
         * @throws RuntimeException if for any reason the copy fails (rare).
         */
        @SuppressWarnings("UnusedReturnValue")
        public <RES> RES into(RES instance) {
            excludeFields.addAll(EXCLUDE_METHODS);
            //If instance of RES, type of instance.getClass() will be Class<RES>, no matter
            //what the compiler says
            @SuppressWarnings("unchecked")
            Class<RES> aClass = (Class<RES>) instance.getClass();
            return copy(aClass, instance, from);
        }

        //*********************************************************************
        // PRIVATE IMPLEMENTATION
        //*********************************************************************

        private <SOURCE, TARGET> TARGET create(Class<TARGET> clazz, SOURCE source) {
            TARGET instance = null;
            try {
                instance = clazz.getConstructor().newInstance();
                return copy(clazz, instance, source);
            } catch (Exception e) {
                throw new KiwiException("Model " + clazz.getSimpleName() + " has no default constructor");
            }
        }

        private <SUPER, T> T copy(Class<T> clazz, T instance, SUPER from) {
            try {
                if (includeFields == null) includeFields = new HashSet<>();
                if (excludeFields == null) excludeFields = new HashSet<>();

                Class<?> sourceClass = from.getClass();

                List<Method> getters = getGetters(sourceClass, includeFields, excludeFields);
                for (Method getter : getters) {
                    Method setter;
                    try {
                        setter = clazz.getMethod(getSetterForGetter(getter), getter.getReturnType());

                    } catch (NoSuchMethodException ignored) {
                        //TODO LOG debug
                        continue;
                    }
                    Object value = getter.invoke(from);
                    if (where.test(value))
                        setter.invoke(instance, value);
                }
                return instance;
            } catch (Exception e) {
                throw new RuntimeException("Something went wrong copying from: " + from.getClass().getSimpleName() + " into class:" + clazz.getSimpleName(), e);
            }
        }

        @NotNull
        private String getSetterForGetter(Method getter) {
            String name = getter.getName();
            return getSetterForGetter(name);
        }

        @NotNull
        private String getSetterForGetter(String name) {
            return name
                    .replaceFirst("^get", "set")
                    .replaceFirst("^is", "set");
        }

        @NotNull
        private String getFieldForGetter(String name) {
            return TextUtil.firstToLower(name
                    .replaceFirst("^get", "")
                    .replaceFirst("^is", ""));
        }

        private List<Method> getGetters(Class<?> sourceClass, Set<String> includeFields, Set<String> excludeFields) {
            return Arrays.stream(sourceClass.getMethods())
                    .filter(m -> shouldCopy(m, includeFields, excludeFields))
                    .collect(Collectors.toList());
        }

        private boolean shouldCopy(Method m, Set<String> includeFields, Set<String> excludeFields) {
            String fieldName = getFieldForGetter(m.getName());
            boolean hasZeroParams = m.getParameterCount() == 0;
            boolean isStatic = Modifier.isStatic(m.getModifiers());
            boolean isIncluded = includeFields.size() == 0 || includeFields.contains(m.getName()) || includeFields.contains(fieldName);

            //Included fields have precedence. This allow us to have a default list of excluded fields but override it
            //if the user wants
            boolean isExcluded = !includeFields.contains(fieldName) && excludeFields.contains(fieldName);

            return isOrGetMethod(m)
                    && hasZeroParams
                    && !isStatic
                    && isIncluded
                    && !isExcluded;
        }

        private boolean isOrGetMethod(Method m) {
            return m.getName().startsWith("get") || m.getName().startsWith("is");
        }

    }
}
