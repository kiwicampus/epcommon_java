package com.kiwicampus.ep.java.util;

import com.kiwicampus.ep.java.exceptions.KiwiException;
import com.kiwicampus.ep.java.exceptions.KiwiUserException;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * General utilities
 * Created by jasonoviedo on 9/27/16.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Util {
    public static final double ONE_CENT = 0.01;
    static private AsyncErrorHandler asyncErrorHandler;

    static public void setAsyncErrorHandler(AsyncErrorHandler handkler) {
        asyncErrorHandler = handkler;
    }

    public static AsyncErrorHandler getAsyncErrorHandler() {
        return asyncErrorHandler;
    }

    @SuppressWarnings("WeakerAccess")
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException("Decimal places must be greater than 0");

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @SuppressWarnings("StaticMethodNamingConvention")
    private static <T, U, R> Iterable<R> zip(Iterable<T> a, Iterable<U> b, BiFunction<T, U, R> zipper) {
        Iterator<T> ai = a.iterator();
        Iterator<U> bi = b.iterator();
        Iterator<R> x = new Iterator<R>() {
            @Override
            public boolean hasNext() {
                return ai.hasNext() && bi.hasNext();
            }

            @Override
            public R next() {
                return zipper.apply(ai.next(), bi.next());
            }
        };
        return () -> x;
    }

    public static DateFormat defaultDateFormatter(String timezone) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss Z");
        sdf.setTimeZone(TimeZone.getTimeZone(timezone != null ? timezone : "UTC"));
        return sdf;
    }

    public static CompletableFuture<Void> runAsync(Runnable r) {
        Throwable t = new Throwable();

        return CompletableFuture.runAsync(() -> {
            try {
                r.run();
            } catch (Exception e) {
                    KiwiException outer = new KiwiException("Exception on async call", e);
                    outer.setStackTrace(t.getStackTrace());
                if (asyncErrorHandler != null) {
                    asyncErrorHandler.onAsyncError(outer);
                }else
                    outer.printStackTrace();
                throw outer;
            }
        });
    }

    public static void runDelayed(@NotNull Runnable block, long millis) {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        block.run();
                    }
                },
                millis
        );
    }

    @Nonnull
    public static <T> T throwRuntime(String message) {
        throw new RuntimeException(message);
    }

    @Nonnull
    public static <T> T throwKiwi(String message) {
        throw new KiwiException(message);
    }

    @Nonnull
    public static <T> T throwUser(String message) {
        throw new KiwiUserException(message);
    }

    public static <T> T getOrElse(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }

    public static <T> T getOrElse(Supplier<T> block, T defaultValue) {
        try {
            T value = block.get();
            return value == null ? defaultValue : value;
        } catch (Throwable ignored) {
            return defaultValue;
        }
    }

    public static <T> T getOrSupply(Supplier<T> block, Supplier<T> defaultValue) {
        try {
            T value = block.get();
            return value == null ? defaultValue.get() : value;
        } catch (Throwable ignored) {
            return defaultValue.get();
        }
    }

    public static boolean isZero(double value) {
        return Math.abs(value) < ONE_CENT;
    }

    public static String dateStr() {
        return dateStr(System.currentTimeMillis());
    }

    public static String dateStr(String timezone) {
        return dateStr(System.currentTimeMillis(), timezone);
    }

    public static String dateStr(long timestamp) {
        return dateStr(timestamp, null);
    }


    public static String dateStr(long timestamp, String timezone) {
        return defaultDateFormatter(timezone).format(new Date(timestamp));
    }

    public static boolean isDifferentDoubles(double p1, double p2) {
        return Math.abs(p1 - p2) > ONE_CENT;
    }

    public static void waitThread(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {

        }
    }

    public interface AsyncErrorHandler {
        void onAsyncError(KiwiException t);
    }
}
