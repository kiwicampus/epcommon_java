package com.kiwicampus.ep.java.exceptions;

import com.kiwicampus.ep.java.util.Util;

import javax.annotation.Nonnull;

/**
 * Created by jasonoviedo on 6/1/16.
 * <p>
 * Used to signal when a user performs an error, like failing to provide data
 * or invoking a function in a wrong state. {@link #printStackTrace()} won't
 * print the entire trace, only the message this exception was created with
 * plus all the messages from this exception causes (or their names if no
 * message is contained)
 */
@SuppressWarnings("unused")
public class KiwiUserException extends KiwiException {
    private static final long serialVersionUID = -5208107212060244236L;

    public KiwiUserException(@Nonnull Throwable error) {
        super(error);
    }
    public KiwiUserException(@Nonnull String error) {
        super(error);
    }

    public KiwiUserException(@Nonnull String s, Exception e) {
        super(s, e);
    }

    @Override
    public void printStackTrace() {
        System.err.println("User error: " + getMessage());
        printMessageRecursive(getCause());
    }

    private void printMessageRecursive(Throwable cause) {
        if (cause == null)
            return;
        System.err.println(Util.getOrElse(cause.getMessage(), cause.getClass().getSimpleName()));
        printMessageRecursive(cause.getCause());
    }
}
