package com.kiwicampus.ep.java.exceptions;

/**
 * Used to signal general errors. This exception is best used to signal programming errors
 */
public class KiwiException extends RuntimeException {

    public KiwiException(Throwable t) {
        super(t);
    }
    public KiwiException(String s) {
        super(s);
    }

    @SuppressWarnings("WeakerAccess")
    public KiwiException(String s, Throwable e) {
        super(s,e);
    }

    public void printStackTrace() {
        super.printStackTrace();
    }
}
